import os, sys, getopt, glob
from PIL import Image


def main(argv):
    cmd = 'image_thumbs -x <size,size> -s <source-directory> -o <output-directory>'
    source_dir = ''
    output_dir = ''
    size = '128,128'

    try:
        opts, args = getopt.getopt(argv,
                                    "hx:s:o:",
                                    ["size=", "source-dir=","output-dir="])
    except getopt.GetoptError as err:
        print(err)
        print(cmd)
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(cmd)
            sys.exit()
        elif opt in ("-x", "--size"):
            size = (arg.split(','))
            size = tuple(map(int, size))
        elif opt in ("-s", "--source-dir"):
            source_dir = arg
        elif opt in ("-o", "--output-dir"):
            output_dir = arg

    print("Source Directory: {0}".format(source_dir))
    print("Output Directory: {0}".format(output_dir))

    if(os.path.isdir(source_dir)):
        if(not os.path.isdir(output_dir)):
            print("Output Directory didn't exist... Creating it.")
            os.makedirs(output_dir)

        for file in os.listdir(source_dir):
            file_name, file_ext = os.path.splitext(file)

            if(file_ext.lower() == ".jpg"
                or file_ext.lower() == ".png"
                or file_ext.lower() == ".gif"):
                print("Processing {0}{1}".format(source_dir, file))

                thumb = "{0}/{1}.thumbnail{2}".format(output_dir,
                                                        file_name,
                                                        file_ext)

                try:
                    im = Image.open("{0}{1}".format(source_dir, file))
                    im.thumbnail(size)
                    im.save(thumb)
                except:
                    print("Cannot create thumbnail for {0}".format(file))
                    print(sys.exc_info())


if __name__ == "__main__":
   main(sys.argv[1:])
