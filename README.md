# image_thumbs.py
Command Line utility to create thumbnails from a directory of images.

This utility is written for Python 3.x and has not been tested with Python 2.x.

## Usage
Simply download and call:
```
python image_thumbs.py -x 256,256 -s /path/to/images/ -o /path/to/images/thumbs
```

### Arguments

- `-x` (`--size`): Comma separated integers. This is the thumbnail size. (h, w)
- `-s` (`--source-dir`): The source directory of images.
- `-o` (`--output-dir`): The directory you want thumbnails to be saved to.
